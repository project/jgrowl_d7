(function($) {
  $.jGrowl.defaults.pool = 5;
  $.jGrowl.defaults.closeTemplate = Drupal.t('Close');
})(jQuery);